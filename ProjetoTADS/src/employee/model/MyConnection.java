package employee.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class MyConnection {
    private Connection connection;

    
    public MyConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://hairsoft.mysql.uhserver.com/hairsoft", "hairsoft", "uninove+1");
        System.out.println("Conectado com sucesso.");
    }
    
    
    
    public static void main(String[] args) throws SQLException {
        try{
            new MyConnection();
        }
        catch(ClassNotFoundException e){
         System.out.println("Não conectado ao banco de dados.");
        }
    }
}
