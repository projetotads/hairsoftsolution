/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee.controller;

import employee.model.ClienteDAO;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Dani
 */
public class ClienteDAOController {
    public List<ClienteDAOController> pesquisarPorNome(String nome)  throws SQLException {

        // Olha que enxuto que ficou. A sua View não se comunicará com o DAO, mas poderá executar os métodos dele através de chamadas do Controller, como esse método aqui.
        ClienteDAO clienteDAO = new ClienteDAO();
        return clienteDAO.pesquisarPorNome(nome);
    }
}
