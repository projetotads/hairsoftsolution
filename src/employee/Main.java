package employee;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
    
    private Stage primaryStage;
    private VBox anchorpane;
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        this.primaryStage = primaryStage;
       
        showMainView();
     }
    
    private void showMainView() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/MainView.fxml"));
        anchorpane = loader.load();
        Scene scene = new Scene(anchorpane);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setTitle("HairSoft Soluções");
     
    }
    
    public void handlecadastrarnovo() throws IOException{
       FXMLLoader loader = new FXMLLoader();
       loader.setLocation(Main.class.getResource("view/cadastrarNovo.fxml"));
       BorderPane cadastrarNovo = loader.load();
    }
    

    public static void main(String[] args) {
        launch(args);
    }
}
